# Status report

- Groupe : Tuteurs
- Date : 12/07/2022

## Ordre du jour

- [Status report](#status-report)
  - [Ordre du jour](#ordre-du-jour)
  - [Plan d'ensemble](#plan-densemble)
  - [Activités](#activités)
  - [Risques et problèmes](#risques-et-problèmes)
  - [Discussions de contenu](#discussions-de-contenu)

## Plan d'ensemble

![Plan](plans/22-07-12.png)

## Activités

| Activités terminées la semaine passée | Activités planifiées la semaine prochaine |
| ------ | ------ |
| Amélioration du prototype | |
| Rédaction d'une 1ère version des documents pour le BAPP | |

## Risques et problèmes

| Impact \ Probability | Medium     | High  | Issue |
| --                   | --         | --    | --    |
|                      |            |       |       |
| **High**             |            |       |       |
|                      | Délai de livraison |       |       |
|                      |            |       |       |
| **Medium**           |            |       |       |

## Discussions de contenu

Le *descriptif court* est discuté et validé. Laurent le finalise et l'envoie au BAPP avant ses vacances. Il terminera aussi le *cahier des charges* et la *grille du prototype*.

Le nouveau prototype est capable de sauter  xx cm avec le tremplin.  Nous incluerons donc le tremplin dans le projet

Michel va vérifier le matériel en stock et mettre en place le dépôt Gitlab du projet.
