# Contenu du kit de démarrage

Le kit fourni contient :

- 2 moteurs DC équipés d'un encodeur en quadrature
- 1 H-bridge [DRI0044](https://wiki.dfrobot.com/2x1.2A_DC_Motor_Driver__TB6612FNG__SKU__DRI0044), capable de contrôler 2 moteurs DC.
- 1 [*line follower sensor*](https://www.robotshop.com/en/dfrobotshop-rover-line-follower-sensor.html)
- 1 module de communication *Bluetooth* [VMA302](https://www.velleman.eu/downloads/29/vma302_a4v02.pdf)
- 1 Arduino Nano Every

## Moteur

Le moteur fourni dans le kit est un moteur à courant continu, équipé d'un encodeur en quadrature et d'un réducteur de vitesse.  
Il dispose de plusieurs points de fixation pour des vis M4.

![moteur](./img/moteur.jpg)

## Spécifications techniques

* Rapport de réduction : 39,6
* Tension nominale : 7,4V
* Courant à vide : 240mA
* Courant en charge: < 750mA
* Vitesse de rotation à vide : 350RPM
* Couple de démarrage : 5kg.cm
* Couple nominal : 800g.cm
* Résolution de l'encodeur : 360 impulsions par tour
